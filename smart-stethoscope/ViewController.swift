//
//  ViewController.swift
//  smart-stethoscope
//
//  Created by hanpanpan200 on 1/25/20.
//  Copyright © 2020 hanpanpan200. All rights reserved.
//

import UIKit
import AVFoundation
import MediaPlayer
import AVKit

class ViewController: UIViewController, AVAudioPlayerDelegate, AVAudioRecorderDelegate {
    
    @IBOutlet weak var recordButton: UIButton!
    @IBOutlet weak var playButton: UIButton!
    
    var audioSession: AVAudioSession!
    var audioRecorder: AVAudioRecorder!
    var audioPlayer: AVAudioPlayer!
    
    var bluetoothInput: AVAudioSessionPortDescription?
    var headphoneOutput: AVAudioSessionPortDescription?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupAudioSession()
        setupRecorder()
        setupRoutePickerView()
        setupButtonEnableStatus()
    }
    
    func setupRoutePickerView() {
        let pickerParentView = UIView(frame: CGRect(x: 0, y: view.bounds.midY, width: view.bounds.width, height: view.bounds.height))
        pickerParentView.backgroundColor = .clear
        let routePickerView = AVRoutePickerView(frame: pickerParentView.bounds)
        pickerParentView.addSubview(routePickerView)
        view.addSubview(pickerParentView)
        view.backgroundColor = .black
    }
    
    func setupRecorder() {
        let recorderSettings = [
            AVFormatIDKey: kAudioFormatAppleLossless,
            AVEncoderAudioQualityKey: AVAudioQuality.max.rawValue,
            AVEncoderBitRateKey: 320000,
            AVNumberOfChannelsKey: 2,
            AVSampleRateKey: 44100.00
        ] as [String : Any]

        do {
            audioRecorder = try AVAudioRecorder(url: getFilePath(), settings: recorderSettings)
            audioRecorder.delegate = self
            audioRecorder.prepareToRecord()
        }
        catch {
            print("Can't Init Audio Recorder: \(error)")
        }
        
    }
    
    func getCacheDirectory() -> String {
        let paths = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)
        return paths[0]
    }
    
    func getFilePath() -> URL {
        let path = (getCacheDirectory() as NSString).appendingPathComponent("audioFile.m4a")
        return NSURL(fileURLWithPath: path) as URL
    }
    
    func setupAudioSession() {
        audioSession = AVAudioSession.sharedInstance()
        
        do
        {
            try audioSession.setCategory(.playAndRecord, mode: .voiceChat, options: [.allowBluetooth, .allowBluetoothA2DP])
            try audioSession.setActive(true)
        }
        catch {
            print("setupAudioSession failed: \(error)")
        }
        
        let outputs = audioSession.currentRoute.outputs.filter({$0.portType == .headphones })
        headphoneOutput = outputs.isEmpty ? nil : outputs[0]
        let inputs = (audioSession.availableInputs ?? audioSession.currentRoute.inputs).filter({$0.portType == .bluetoothHFP })
        bluetoothInput = inputs.isEmpty ? nil : inputs[0]
        
        do
        {
            try audioSession.setPreferredInput(bluetoothInput)
        }
        catch
        {
            print("audioSession.setPreferredInput failed \(error)")
        }
    }
    
    func setupButtonEnableStatus() {
        let isEnabeld = headphoneOutput != nil && bluetoothInput != nil
        recordButton.isEnabled = isEnabeld
        playButton.isEnabled = isEnabeld
    }
    
    @IBAction func recordAudio(_ sender: UIButton) {
        if sender.titleLabel?.text == "Record" {
            sender.setTitle("Stop", for: .normal)
            playButton.isEnabled = false
            
            audioRecorder.record()
        } else {
            sender.setTitle("Record", for: .normal)
            playButton.isEnabled = true
            
            audioRecorder.stop()
        }
    }
    
    
    @IBAction func playAudio(_ sender: UIButton) {
        if sender.titleLabel?.text == "Play" {
            sender.setTitle("Stop", for: .normal)
            recordButton.isEnabled = false
            
            setupPlayer()
            audioPlayer.play()
        } else {
            sender.setTitle("Play", for: .normal)
            
            audioPlayer.stop()
        }
    }
    
    
    func setupPlayer() {
        do {
            try audioPlayer = AVAudioPlayer(contentsOf: getFilePath())
            audioPlayer.delegate = self
            audioPlayer.prepareToPlay()
            audioPlayer.volume = 1.0
        } catch {
            print("Can't Prepare Audio Player: \(error)")
        }
    }
    
    func audioRecorderDidFinishRecording(_ recorder: AVAudioRecorder, successfully flag: Bool) {
        playButton.isEnabled = true
    }
    
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        recordButton.isEnabled = true
    }
    
}

